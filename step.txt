1 Passo CRIAR A ENV
 * virtualenv env
2 Passo ATIVAR ENV
 * source NOME_ENV/bin/activate - linux
 * \NOME_ENV\scripts\activate - windows
3 INSTALAR DJANGO
 * pip install django
4 CRIAR PROJETO DJANGO
 * django-admin startproject myapp
5 RODAR SERVIDOR DJANGO
 * python manage.py runserver
6 CRIAR TABELAS DO BANCO DE DADOS
 * python manage.py makemigrations
 * python manage.py migrate
7 CRIAR APP
 * django-admin startapp estacionamento
8 LINKAR APP AO SETTINGS
9 INSTALAR LIB DE IMAGENS
 * pip install pillow
10 Criar super usuario
 * python manage.py createsuperuser
11 SERIALIZER 
    criar um arquivo serializer e adicionar as classes linkando com os models
12 VIEW
    Importar serializer
    Criar class view para buscar dados
13 URLS
    Importar classe de view
    Criar rota da view

# GIT

OBS: cria um arquivo .gitignore e joga um template do django lá

git init
git add .
git commit -m "projeto criado"
git status
git pull origin main
git remote add origin git@gitlab.com:matheusjoselfm/api-estacionamento.git

git checkout -b main

docker ps
docker stats
docker logs IDCONTAINER
docker-compose -f deployments/docker-compose.desenvolvimento.yml --compatibility up --build


Orlando Moura
​boa tarde, Matheus como está o kubernetes no atual mercado, você poderia me responder?

Yago Prazim
​Matheus, você já está há quanto tempo na área TI?
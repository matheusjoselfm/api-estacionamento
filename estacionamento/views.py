from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, serializers, generics

from .models import Estacionamento, Veiculo

from estacionamento.api.serializers import EstacionamentoSerializer, VeiculoSerializer

class VeiculoView(APIView):
    """
    Lista todos os veiculos e cria um novo.
    """

    def get(self, request, format=None):
        veiculo = Veiculo.objects.all()
        print(veiculo)
        serializer = VeiculoSerializer(veiculo, many=True)
        print(serializer.data)
        return Response(serializer.data)

    def post(self, request, format=None):

        if Veiculo.objects.filter(**request.data).exists():
            raise serializers.ValidationError('Veículo já existe')

        serializer = VeiculoSerializer(data=request.data)


        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
class VeiculoViewDetalhe(APIView):
    """
    Obtem, atualiza ou deleta uma instancia de veiculo.
    """

    def get_object(self, pk):
        try:
            return Veiculo.objects.get(pk=pk)
        except Veiculo.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        veiculo = self.get_object(pk)
        serializer = VeiculoSerializer(veiculo)
        return Response(serializer.data)

    def delete(self, request, pk, format=None):
        veiculo = self.get_object(pk)
        veiculo.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def put(self, request, pk, format=None):
        veiculo = self.get_object(pk)
        serializer = VeiculoSerializer(veiculo, data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class EstacionamentoView(generics.ListCreateAPIView):
    queryset = Estacionamento.objects.all()
    serializer_class = EstacionamentoSerializer

    def post(self, request, *args, **kwargs):

        veiculo = Veiculo.objects.get(id=request.data.get('veiculo'))
        veiculo_estacionado = Estacionamento.objects.filter(veiculo=veiculo, data_saida__isnull=True)
        if veiculo_estacionado.exists():
            raise serializers.ValidationError('Veículo possui entrada aberta')
        
        return self.create(request, *args, **kwargs)


class EstacionamentoViewDetalhe(generics.RetrieveUpdateDestroyAPIView):
    queryset = Estacionamento.objects.all()
    serializer_class = EstacionamentoSerializer

class VeiculosEstacionadosView(APIView):
    """
    Lista todos os veiculos estacionados em aberto.
    """

    def get(self, request, format=None):
        veiculos_estacionados = Estacionamento.objects.filter(
            data_saida__isnull=True
        ).values(
            'data_entrada', 'data_saida',
            'veiculo__nome_cliente', 'veiculo__modelo_carro', 'veiculo__placa',
            'veiculo__cor', 'veiculo__foto',
            'veiculo'
        )

        return Response(veiculos_estacionados)


class EstacionadosCompletoView(APIView):
    """
    Lista todos os veiculos estacionados em aberto.
    """

    def get(self, request, format=None):
        veiculos_estacionados = Estacionamento.objects.all(
        ).values(
            'data_entrada', 'data_saida',
            'veiculo__nome_cliente', 'veiculo__modelo_carro', 'veiculo__placa',
            'veiculo__cor', 'veiculo__foto',
            'veiculo'
        )

        return Response(veiculos_estacionados)




